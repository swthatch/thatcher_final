﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Class enemy script GameObject player;
    // Class enemy scriptRigidbody rb;
    public float speed;

    public float stoppingDistance;

    public float retreatDistance;

    private float timeBtwShots;
    public float startTimeBtwShots;

    public GameObject projectile;
    public Transform player;


    // Start is called before the first frame update
    void Start(){
        // Class enemy scriptplayer = GameObject.FindGameObjectWithTag("Player");
        // Class enemy scriptrb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        timeBtwShots = startTimeBtwShots;

    }

    // Class enemy scriptprivate void FixedUpdate()
    // Class enemy script{
    // Class enemy scriptVector3 dirToPlayer = player.transform.position - transform.position;
    // Class enemy scriptdirToPlayer = dirToPlayer.normalized;

    // Class enemy scriptrb.AddForce(dirToPlayer * speed);
    // Class enemy script}

    // Update is called once per frame
    void Update(){

    if(Vector3.Distance(transform.position, player.position) > stoppingDistance){

    transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);

    } else if (Vector3.Distance(transform.position, player.position) < stoppingDistance && Vector3.Distance(transform.position, player.position) > retreatDistance){

    transform.position = this.transform.position;

    } else if (Vector3.Distance(transform.position, player.position) < retreatDistance){

    transform.position = Vector3.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);

    }

    if(timeBtwShots <= 0){

            Instantiate(projectile, transform.position, Quaternion.identity);
            timeBtwShots = startTimeBtwShots;
    
    
    } else {

            timeBtwShots -= Time.deltaTime;

    }

    }
}
