﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public float health = 50f;

    public int count;

    public void TakeDamage (float amount)
    {
        health -= amount;
        if (health <= 0f)
        {
            Die();
        }
    }

    void Die()
    {
        //count += 1;
        //print("added 1 to count");
        //if (count == 2)
        //{
        //    print("Reached 2 on count");
        //    Destroy(GameObject.FindWithTag("Wall"));
        //}
        Destroy(gameObject);
    }

    //void Wall_Destroy()
    //{
    //Destroy(gameObject.FindWithTag("Wall"));
    //}
}
